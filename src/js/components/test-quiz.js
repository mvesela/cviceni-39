import { __removeClass, __toggleClass } from '../lib/utils';
import Component from './component';


export default class TestQuiz extends Component {

  constructor(id, element, opts) {
    super(element);

    this.id = id;

    this.opts = { ...TestQuiz.defaults, ...opts };

    this.$questions = this.target.querySelectorAll(`.${this.opts.itemClass}`);
    this.cache = {
      answersCorrect: 0,
      colors: [],
      questions: [],
    };

    // get colors for all feedback variants
    if (this.opts.zpetnaVazba) {
      this.cache.colors = this.opts.zpetnaVazba.map((feedback) => feedback.barva);
    }
  }

  init() {
    this._registerHandlers();
  }

  _registerHandlers() {

    this.$questions.forEach(($question, questionIndex) => {
      // all questions are without answer by default
      this.cache.questions.push({ answered: false });

      const answerCorrect = parseInt($question.getAttribute('data-module-answer'), 10);
      const $answers = $question.querySelectorAll(`.${this.opts.optionClass}`);

      $answers.forEach(($answer, answerIndex) => {
        $answer.addEventListener('click', () => {
          const $answerCurrent = $answer;

          // if already selected do nothing
          if ($answerCurrent.classList.contains('selected')) {
            return;
          }

          // remove the selection
          $answers.forEach(($answersItem) => __removeClass($answersItem, this.opts.selectedClass));
          // select the clicked one
          __toggleClass($answerCurrent, this.opts.selectedClass);

          // get sum of all corect answers
          if (this.opts.zpetnaVazba) {
            if (answerIndex === answerCorrect) {
              this.cache.answersCorrect += 1;
            } else if (this.cache.answersCorrect !== 0 && this.cache.questions[questionIndex].answered === true) {
              this.cache.answersCorrect -= 1;
            }
            // loop through all "zpetnaVazba" to get the one with the fullfilled condition
            const feedbackActual = this.opts.zpetnaVazba.find((feedback) => feedback.podminka.includes(this.cache.answersCorrect));

            // show the right feedback message
            this.$buttonFeedback.classList.remove(...this.cache.colors);
            this.$buttonFeedback.classList.add(feedbackActual.barva);
            this.$buttonFeedbackText.innerText = feedbackActual.text;
          }

          // note that this question has been answered (whether true or false)
          this.cache.questions[questionIndex].answered = true;

          // show OK button with feedback only when all question have been answered
          if (this.cache.questions.find((question) => !question.answered) === undefined) {
            this.showFeedback();
          }

        }, false);
      });
    });
  }
}

TestQuiz.defaults = {
  itemClass: 'test-quiz-item',
  optionClass: 'option',
  selectedClass: 'selected',
};
