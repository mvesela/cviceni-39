import AudioPlayer from './audio-player';
import { getSecondsMinutesFormat } from './media';

export default class AudioPlayerSimple extends AudioPlayer {
  constructor($element) {
    super($element);

    this.$audio = this.target.querySelector('audio');
    this.$timeLeft = this.target.querySelector('[data-audio-time-left]');
    this.$duration = this.target.querySelector('[data-audio-time-duration]');

    this.$duration.innerText = `/${getSecondsMinutesFormat(this.$audio.duration)}`;

    AudioPlayer.addEventListeners(this.target, this.showFeedback);
  }
}
